import React from 'react';
import {
  AppRegistry,
  Text,
} from 'react-native';
import { StackNavigator } from 'react-navigation';

import DashboardPage from './DashboardPage';
import MyCurrentSavingsDetail from './MyCurrentSavingsDetail';

const WMM = StackNavigator({
  DashboardPage: { screen: DashboardPage },
	MyCurrentSavingsDetail: { screen: MyCurrentSavingsDetail}
});

AppRegistry.registerComponent('WMM', () => WMM);
