'use strict';

import React, { Component } from 'react'
import {
	View,
  TouchableHighlight,
	Text,
	TextInput,
	Keyboard
} from 'react-native';
import { StackNavigator } from 'react-navigation';

class MyCurrentSavingsDetail extends Component {
  static navigationOptions = {
    title: 'My current savings',
  };

  render() {
    return (
      <View>
        <Text style={{textAlign:'center'}}>My current savings</Text>
      </View>
    );
  }
}

module.exports = MyCurrentSavingsDetail;
